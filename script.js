const removeConfirmation = () => {
	document.getElementById('overlay').remove();
};

const setResult = (text) => {
	document.getElementById('result').innerText = text;
};

const addEventListenerToButtons = () => {
	const yes = document.getElementById('btn-yes');
	yes.addEventListener('click', () => {
		removeConfirmation();
		setResult('You just clicked "Yes"!');
	});

	const cancel = document.getElementById('btn-cancel');
	cancel.addEventListener('click', () => {
		removeConfirmation();
		setResult('You just clicked "Cancel"!');
	});
};

const confimation = (message) => {
	// Clear the current result
	setResult('');

	// Create the overlay
	const overlay = document.createElement('div');
	overlay.setAttribute('id', 'overlay');

	// Create the confirmation box
	const confimationContainer = document.createElement('div');
	confimationContainer.setAttribute('id', 'confirmation-container');

	// Create the confirmation message
	const p = document.createElement('p');
	p.setAttribute('id', 'message');
	const pText = document.createTextNode(message);
	p.appendChild(pText);

	// Create a container for the buttons
	const btnContainer = document.createElement('div');
	btnContainer.setAttribute('id', 'btn-container');

	// Create the yes button
	const yes = document.createElement('button');
	yes.setAttribute('id', 'btn-yes');
	yes.innerHTML = 'Yes';

	// Create the cancel button
	const cancel = document.createElement('button');
	cancel.setAttribute('id', 'btn-cancel');
	cancel.innerHTML = 'Cancel';

	// Get the main container to append the elements to
	const app = document.getElementById('app');

	// Build the confirmation dialog
	btnContainer.appendChild(yes);
	btnContainer.appendChild(cancel);
	confimationContainer.appendChild(p);
	confimationContainer.appendChild(btnContainer);
	overlay.appendChild(confimationContainer);

	// Append to the main container
	app.appendChild(overlay);

	// Add the eventlisteners
	addEventListenerToButtons();
};
